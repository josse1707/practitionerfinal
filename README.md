+NOTES APP.
+Esta montado en una instancia de Amazon Web Services EC2, por lo cual no podra visualizar el contenido del proyecto
+sin las instrucciones necesaricias.
+
+### instrucciones ###
+
+*Descargue el repositorio o clonelo en una carpeta previamente creada por usted.
+*En un editor de texto, abra el archicvo con el nombre database.js
+*dentro de este documento, se encuentran comentarizadas varias lineas con instrucciones 
+*Para correr el proyecto de manera local, descomentarize la linea "const MONGODB_URI = `mongodb://localhost/mean-crud`;"
+*comente la linea "const MONGODB_URI = `mongodb://mongo:27017/mean-crud`;"
+*una vez realizados y guardados estos cambios en el documento database.js abra una consola de Windows
+*posicionese dentro de la carpeta donde clono el repositorio
+*una vez dentro de la carpeta instale las dependencias con el comando npm install
+*cuando el proceso termine, ejecute el comando npm start para iniciar el proyecto de manera local
+*hecho esto y corroborando que no surgio ningun error en la consola, abra un navegador
+*dentro del navegador, coloce localhost:4000 para poder acceder a la plataforma notesapp
+
+### Instruciones con EC2 ###
+
+*para poder abrir este proyecto con EC2, no es necesario modificar nada dentro del proyecto.
+*el administrador de la instancia de AWS, es el unico con acceso a la consola para poder obtener
+la ip y la llave con la que puede iniciar sesion desde el aplicativo PUTTY
+*no se incluye la llave ni la ip del proyecto, ya que AWs, por seguridad cambia de manera dinamica la ip, por 
+tanto el usuario externo a la plataforma, no podra iniciar con exito el proyecto mas que de manera local
+*dentro de putty, se accede a la consola del servidor por SSH.
+*esto permite realizar diferentes cambios en el proyecto o solo el levantamiento den el entorno de AWS.
+*dentro de la consola de la isntancia creada, se debe pocicionare en la ruta Tech> practitionerfinal
+*dentro de esta ruta, cosntruimos el contenedor con el comando "sudo docker-compose build"
+*cuando el proceso termine, ejecutar el comando sudo docker-compose up
+*estos comandos, nos ayudaran a visualizar el proyecto dockerizado desde la instancia EC2 desde cualquier
+dispositivo mentiante la IP publica que otorgue en ese momento AWS.
+*un el navegador de cualquier dispositivo coloque la IP seguida del puerto 4000"xx.xx.xx.xx:4000", de esta manera
+podra interactuar con la plataforma sin necesidad de estar en su entrono local.